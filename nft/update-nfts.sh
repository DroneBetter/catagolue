#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"
cd ..

output_dir="src/main/webapp/autogen/nfts"
mkdir "$output_dir"

set -e

python3 nft/prepare_nfts.py nft/all_nfts.json "$output_dir"
cat "initialise/nft-mapping.txt"
cat "initialise/reverse-nft-mapping.txt"
