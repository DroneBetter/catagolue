#!/bin/bash

HASH="$( sha256sum /tmp/client-secret.json )"
HASH="${HASH:0:64}"
printf "Hash: \033[32;1m $HASH \033[0m\n"

if [ "$HASH" != "fe871d4726d0f849477eac1755833a334c8323be6a6a84e5d7f915f7e174416b" ]; then
echo "Warning: skipping Catagolue update as client secret is incorrect"
exit 0
fi

# Ensure the directory does not exist so we can clone into it:
rm -rf "Shinjuku"
rm -rf "initialise/shinjuku"

set -e

git clone "$1"
cp -r "Shinjuku/shinjuku" "initialise/shinjuku"

HASH="$( sha512sum /tmp/client-secret.json )"
HASH="${HASH:0:40}"
# Do NOT print the hash here; it should be secret!
cd initialise
python3 -u contrsynth.py "$HASH" "https://catagolue.hatsya.com"
python3 -u diffupdate.py "$HASH" "https://catagolue.hatsya.com"
python3 -u contrsynth.py "$HASH" "https://catagolue.hatsya.com"
