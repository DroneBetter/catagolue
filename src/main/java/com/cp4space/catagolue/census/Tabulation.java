package com.cp4space.catagolue.census;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import java.io.BufferedReader;
import java.io.StringReader;
import java.util.logging.Logger;
import java.util.Date;
import java.util.ArrayList;
import java.util.Map;

public class Tabulation {

    private static final Logger log = Logger.getLogger(Census.class.getName());

    static int MAX_BYTES  = 960000;
    static int BIG_ENOUGH = 800000;

    String prefix;
    public StringBuilder dataBuilder;
    double compression;
    int existingLength;
    int compressionMode;
    int triggerLength;

    final ArrayList<Blob> existingBlobs;

    public Tabulation(String apgPrefix) {
        existingBlobs = new ArrayList<Blob>();
        prefix = apgPrefix;
        dataBuilder = new StringBuilder();
        existingLength = 0;

        compressionMode = 0;
        triggerLength = MAX_BYTES;
    }


    /**
     * Appends a row to the tabulation.
     * @param apgcode  the representation of the object, such as "xp15_4r4z4r4"
     * @param quantity  the number of occurrences of the object
     */
    public void appendRow(String apgcode, long quantity) {

        if (dataBuilder.length() >= triggerLength) {

            String datastring = dataBuilder.toString();
            byte[] comp = GzipUtil.zip(datastring, "gz");

            if (comp.length >= BIG_ENOUGH) {
                log.warning("Tabulation " + prefix + ": Fragment " + existingBlobs.size() + ": " +
                            datastring.length() + " bytes uncompressed; " + comp.length + " bytes compressed.");

                existingBlobs.add(new Blob(comp));
                existingLength += datastring.length();
                dataBuilder.setLength(0);
                dataBuilder = new StringBuilder();
                compressionMode = 0;
                triggerLength = MAX_BYTES;
            } else {
                compressionMode = 1;
                triggerLength = dataBuilder.length() + (MAX_BYTES - comp.length);
            }
        }

        // Append the row:
        if (dataBuilder.length() != 0) { dataBuilder.append("\n"); }
        dataBuilder.append(apgcode + " " + String.valueOf(quantity));
    }

    void saveFragments(Key parent, int fragments) {

        ArrayList<Entity> entities = new ArrayList<Entity>();

        for (int i = 0; i < fragments; i++) {
            Entity entity = new Entity("Fragment", "frag" + i, parent);
            entity.setProperty("compressed", true);
            entity.setProperty("contents", existingBlobs.get(i));
            entities.add(entity);
        }

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        datastore.put(entities);
    }

    public int setArbitraryData(Entity tab, String prop) {

        String datastring = dataBuilder.toString();

        int fragments = existingBlobs.size();

        if (fragments > 0) {
            saveFragments(tab.getKey(), fragments);
        }

        tab.setProperty("fragments", fragments);

        if (compressionMode == 0) {
            tab.setProperty("compressed", false);
            tab.setProperty(prop, new Text(datastring));
        } else {
            byte[] comp = GzipUtil.zip(datastring, "gz");
            tab.setProperty("compressed", true);
            tab.setProperty(prop, new Blob(comp));
        }
        tab.setProperty("lastModified", new Date());

        return existingLength + datastring.length();
    }

    public int writeTab(Entity tab) { return setArbitraryData(tab, "censusData"); }

    public static String getArbitraryDataInternal(Entity tab, String prop) {

        Object tprop = tab.getProperty(prop);
        boolean compressed = false;

        if (tprop instanceof Text) {
            compressed = false;
        } else if (tprop instanceof Blob) {
            compressed = true;
        } else {
            Boolean gzipped = (Boolean) tab.getProperty("compressed");
            compressed = (gzipped != null) && (gzipped == true);
        }

        if (compressed) {
            byte[] comp = ((Blob) tprop).getBytes();
            return GzipUtil.unzip(comp);
        } else {
            return ((Text) tprop).getValue();
        }
    }

    public static String getArbitraryData(Entity tab, String prop) {

        Object f = tab.getProperty("fragments");

        int fragments = (f == null) ? 0 : Integer.valueOf(f.toString());

        if (fragments == 0) {
            return getArbitraryDataInternal(tab, prop);
        }

        StringBuilder sb = new StringBuilder();

        ArrayList<Key> fragkeys = new ArrayList<Key>();
        for (int i = 0; i < fragments; i++) {
            fragkeys.add(tab.getKey().getChild("Fragment", "frag"+i));
        }

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Map<Key, Entity> entityMap = datastore.get(fragkeys);
        for (int i = 0; i < fragments; i++) {
            sb.append(getArbitraryDataInternal(entityMap.get(fragkeys.get(i)), "contents"));
            sb.append("\n");
        }

        sb.append(getArbitraryDataInternal(tab, prop));

        return sb.toString();

    }

    public static String getCensusData(Entity tab) {
        return getArbitraryData(tab, "censusData");
    }

    public static BufferedReader getCensusStream(Entity tab) {

        Long fragments = (Long) tab.getProperty("fragments");
        if ((fragments != null) && (fragments > 0)) {
            return new BufferedReader(new StringReader(getCensusData(tab)));
        }

        Boolean gzipped = (Boolean) tab.getProperty("compressed");

        if ((gzipped == null) || (gzipped == false)) {
            String tabulationData = ((Text) tab.getProperty("censusData")).getValue();
            return new BufferedReader(new StringReader(tabulationData));
        } else {
            byte[] comp = ((Blob) tab.getProperty("censusData")).getBytes();
            return GzipUtil.uncompressStream(comp);
        }
    }
}
